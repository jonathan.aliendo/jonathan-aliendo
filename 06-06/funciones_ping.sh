#!/bin/bash

#Declaración de funciones

func_manual () {

	#Manual de uso
	echo " "
	echo "Manual de uso:"
	echo " "
	echo "Para usar correctamente este script debe ingresar:"
	echo "Cantidad de ping a realizar: debe ingresar el argumento -C seguido de un numero entero positivo"
	echo "Protocolo deseado: Debe ingresar el argumento -p seguido de 4 (en caso de ser ipv4) o 6 (en caso de ser ipv6)"
	echo "Hacer ping a dominio de difusión: debe ingresar -b"
	echo "Tener una marca de tiempo: debe ingresar -T"
	echo "Debe ingresar un Hostname, Dominio (example.com) dirección IP valida (0.0.0.0)"
	exit 1
}

func_entero () {

	#Función que compara si un numero es entero positivo
	numero=$1
	if [[ $numero =~ ^[0-9]{1,}$ ]] && [[ $numero > "0" ]]; then
		return 0
	else
		return 1
	fi
}

func_proto () {

	#Función para verificar que se introduzca el protocolo correcto
	proto=$1
	if [[ "$proto" -eq 4 ]] || [[  "$proto" -eq 6 ]]; then
		return 0
	else
		return 1
	fi
}

func_hostname () {

	#Función que verificar si el hostname es valido
	host=$1
	if [[ $host =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
		OIFS=$IFS
		IFS="."
		hostip=(${host})
		IFS=$OIFS

		if [[ ${hostip[0]} -le 255 && ${hostip[1]} -le 255 && ${hostip[2]} -le 255 && ${hostip[3]} -le 255 ]]; then
			return 0
		fi
	elif [[ $host =~ ^([a-zA-Z0-9](([a-zA-Z0-9]){0,67}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,}$ ]] || [[ $host =~ ^([a-zA-Z0-9](([a-zA-Z0-9]){0,67}[a-zA-Z0-9])) ]]; then
		return 0
	else
		return 1
	fi
}

#Inicio del script

args=($@)
for opcion in "${!args[@]}"; do

        case ${args[$opcion]} in

                -C) cantidad=${args[$(($opcion+1))]}
		    if func_entero $cantidad; then
                    	counter="-c $cantidad"
		    else
		   	echo "Ha ingresado un valor invalido en la opción de cantidad de ping, debe ingresar un entero positivo mayor que 0."
		        func_manual
                    fi
                ;;
                -T) timestamp="-D"
                ;;
                -p) proto=${args[$(($opcion+1))]}
                    if func_proto $proto; then
		    	p="-$proto"
		    else
			echo "Debe introducir 4 o 6 que son los protocolos validos"
                        func_manual
		    fi
                ;;
                -b) b="-b"

                ;;
	esac
done

if func_hostname ${args[-1]}; then
	ping $b $timestamp $p $counter ${args[-1]}
else
	echo "Debe ingresar un destino valido, verifique la dirección IP o Hostname"
	func_manual
fi

