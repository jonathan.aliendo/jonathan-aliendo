#!/bin/bash

#Variable a usar para el nombre de los backups
dt=`date +%y%m%d`
#
while true
do
echo "MENÚ DE OPCIONES"
echo "1		Backup a directorio local"
echo "2		Backup a host"
echo "3		Generar y enviar llave para conexion remota"
echo "4		Ya posee llave, enviar a host remoto"
echo "5		Crear crontab para el script"
echo "6		Salir del Script"

read -p "Escriba su opcion: " respuesta1

case $respuesta1 in
#Caso 1: Backup a directorio local
	1)
		echo "MENÚ DE OPCIONES"
                echo "1       /home/$USER"
                echo "2       /etc/"
                echo "3       otro"
                read -p "Escriba su opcion: " respuesta2
                read -p "Escriba direcorio destino (debe terminar con /): " dir_dest
                case $respuesta2 in
                        1)
			   dir_or="/home/$USER"
                           rsync -artvu $dir_or $dir_dest$USER-$dt.back
                        ;;
                        2)
			   dir_or="/etc/"
                           rsync -artvu $dir_or $dir_dest$USER-$dt.back
                        ;;
                        3)
			   read -p "Introducir directorio origen: " dir_or
			   rsync -artvu $dir_or $dir_dest$USER-$dt.back
			;;
			*)
			   echo "Debe escoger una opción válida"
          		   exit 1
        		;;
		esac
	;;
#Caso 2: Backup a host
	 2)
		echo "MENÚ DE OPCIONES"
                echo "1       /home/$USER"
                echo "2       /etc/"
                echo "3       otro"
		echo "En este caso el Backup se creara en el directorio /tmp/ del localhost"
                read -p "Escriba su opcion: " respuesta3
                case $respuesta3 in
                        1)
			   dir_or="/home/$USER"
                           rsync -artvu $dir_or $USER@localhost:/tmp/$USER-$dt.back
                        ;;
                        2)
			   dir_or="/etc/"
                           rsync -artvu $dir_or $USER@localhost:/tmp/etc-$dt.back
                        ;;
                        3)
			   read -p "Introducir directorio origen: " dir_or
                           rsync -artvu $dir_or $USER@localhost:/tmp/etc-$dt.back
                        ;;
                        *)
			   echo "Debe escoger una opción válida"
                           exit 1
                        ;;
		esac
	;;
#Caso 3: Generar y enviar llave para conexion remota
	3)
	   ssh-keygen -t rsa
	   ssh-copy-id -i ~/.ssh/id_rsa.pub $USER@localhost
	;;
#Caso 4:  Ya posee llave, enviar a host remoto
	4)
	   ssh-copy-id -i ~/.ssh/id_rsa.pub $USER@localhost
	   echo "Llave enviada"
	;;
#Caso 5: Crear crontab
	5)
                echo "MENÚ DE OPCIONES"
                echo "1       Ejecutar todos los dias a las 11PM"
                echo "2       Ejecutar todos los dias a las 5AM y 11PM"
                echo "3       Ejecutar domingos a las 11PM"
                echo "4       Ejecutar de lunes a viernes a las 5AM y 11Pm"
                read -p "Escriba su opcion: " respuesta4
                case $respuesta4 in
                        1)
                           sudo echo "0 23 * * * $PWD/back.sh" >> /var/spool/cron/crontabs/$USER
                        ;;
                        2)
                           sudo echo "0 5,23 * * * $PWD/back.sh" >> /var/spool/cron/crontabs/$USER
                        ;;
                        3)
                           sudo echo "0 23 * * 0 $PWD/back.sh" >> /var/spool/cron/crontabs/$USER
                        ;;
                        4)
                           sudo echo "0 5,23 * * 1-5 $PWD/back.sh" >> /var/spool/cron/crontabs/$USER
                        ;;
                        *)
                           echo "Debe escoger una opción válida"
                           exit 1
                        ;;
		esac

	;;
#Caso 6: Salir del script
	6)
	   echo "Hasta luego"
	   exit 0
	;;

        *)
          echo "Debe escoger una opción válida"
	  echo " "
        ;;

esac

done
