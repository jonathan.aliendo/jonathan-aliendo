#!/bin/bash

args=( $@ )
for opcion in "${!args[@]}"; do

	case ${args[$opcion]} in

		-C) cantidad=${args[$(($opcion+1))]}
		    entero='^[0-9]+$'
		    if ! [[ $cantidad =~ $entero ]] ; then
   			echo "ERROR: No es un número entero positivo"; exit 1
		    else
		    	counter="-c $cantidad"
		    fi
		;;
		-T) timestamp="-D"
		;;
		-p) proto=${args[$(($opcion+1))]}
		   if  [[ "$proto" -eq "-4" || "$proto" -eq "-6" ]]; then
		  	 p="$proto"
		   else
			echo "$proto no es un protocolo"
			exit 1
		   fi
		;;
		-b) b="-b"

		;;
	esac
done
ping $b $timestamp $p $counter ${args[-1]}
