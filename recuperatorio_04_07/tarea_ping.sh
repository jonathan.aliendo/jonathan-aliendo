#!/bin/bash

#Validacion de cantidad de argumentos
if [[ $# -lt 2 ]]; then
	echo "Cantidad de argumentos infuficiente"
	cat Manual.txt
	exit 1
fi

args=( $@ )

for opcion in "${!args[@]}"; do

	case ${args[$opcion]} in

		-C | -c) cantidad=${args[$(($opcion+1))]}
		    	 if [[ $cantidad =~ ^[0-9]{1,}$ ]] && [[ $cantidad -ge "1" ]]; then
               	   		counter="-c $cantidad"
        	    	 else
				echo "Ha ingresado un valor invalido en la opción de cantidad de ping, debe ingresar un entero positivo mayor que 0."
		         	cat Manual.txt
				exit 1
        	    	 fi
		;;
		-T | -t) timestamp="-D"
		;;
		-P | -p) proto=${args[$(($opcion+1))]}
		   	 if  [[ "$proto" -eq "4" || "$proto" -eq "6" ]]; then
		  	 	p="$proto"
		   	 else
				echo "$proto no es un protocolo, debe ingresar 4 o 6"
				cat Manual.txt
				exit 1
		   	 fi
		;;
		-b) b="-b"

		;;
	esac
done
#Validacion ipv4
if [[ ${args[-1]} =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS="."
        hostip=(${args[-1]})
        IFS=$OIFS

        if [[ "${#hostip[@]}" -eq "4" ]]; then
                if [[ ${hostip[0]} -le 255 && ${hostip[1]} -le 255 && ${hostip[2]} -le 255 && ${hostip[3]} -le 255 ]]; then
                        ping $b $timestamp $p $counter ${args[-1]}
                else
                        echo "Debe ingresar un destino valido, verifique la dirección IP o Hostname"
                        cat Manual.txt
                        exit 1
                fi
        fi
#Validacion ipv6
elif [[ ${args[-1]} =~ ([0-9a-fA-F]{0,4}:){1,7}([0-9a-fA-F]){0,4} ]]; then
		ping $b $timestamp $p $counter ${args[-1]}
#Validacion de host
elif [[ ${args[-1]} =~ ^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$ ]]; then
                ping $b $timestamp $p $counter ${args[-1]}
else
                echo "Debe ingresar un destino valido, verifique la dirección IP o Hostname"
                cat Manual.txt
                exit 1
fi

