#!/bin/bash

if [ -f $1 ]
then
	usuario=$(whoami)
	propietario=$(ls -l $1 | awk '{print $3}')
	echo "El fichero $1 existe"
	ejecucion=$(ls -l $1)
	if [ "${ejecucion:3:1}" == "x" ]
	then
		echo "$ejecucion"
		read -p "Desea ejecutarlo? [y/n]" ANS
		if [[ $ANS =~ [yY] ]]; then
			echo "Se esta ejecutando"
		else
			exit 1
		fi
	else
		if [ "$propietario" == "$usuario" ]; then
			echo "No tiene permiso de ejecucion"
			read -p "Desea agregar permisos de ejecucion? [y/n]" ANS
			if [[ $ANS =~ [yY] ]]; then
				chmod +x $1
				ls -l $1
			else
				exit 1
			fi
		else
			echo "No tiene permiso de ejecucion y no es dueño del fichero"
		fi
	fi
else
	echo "El fichero $1 no existe"
fi
